/**
 * Created by Fara Aileen on 7/4/2016.
 */
(function() {
    "use strict";
    var toDoApp = angular.module("toDoApp", []);
    var toDoController = function($log) {
        var toDo = this;
        toDo.item = "";
        toDo.completed = false;
        toDo.list = [];
        toDo.count = 0;
        toDo.addToList = function() {
            toDo.list.push({
                item: toDo.item,
                completed: toDo.completed
            });
            $log.info("Added " + toDo.item + " which is " + toDo.completed + " to list");
            toDo.item = "";
            toDo.completed = false;
            toDo.count++;
        };

        toDo.changeStatus = function(idx) {
            if(toDo.list[idx].completed){
                toDo.count++;
            }else{
                toDo.count--;
            }
            toDo.list[idx].completed = !toDo.list[idx].completed;
        };
    };
    toDoApp.controller("toDoController", ["$log", toDoController]);
})();